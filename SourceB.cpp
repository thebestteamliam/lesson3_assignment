#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <conio.h>
#include <vector>
using namespace std;

unordered_map<string, vector<string>> results;
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);
	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);
	printTable();
	printf("-----------------------\n");
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	if (carPurchase(1, 7, db, zErrMsg))
	{
		printf("buying succeeded\n");
	}
	else
	{
		printf("buying failed\n");
	}
	if(carPurchase(2, 7, db, zErrMsg))
	{
		printf("buying succeeded\n");
	}
	else
	{
		printf("buying failed\n");
	}
	if(carPurchase(12, 5, db, zErrMsg))
	{
		printf("buying succeeded\n");
	}
	else
	{
		printf("buying failed\n");
	}
	clearTable();
	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);
	printf("-----------------------\n");
	printTable();


	balanceTransfer(1, 2, 1000, db, zErrMsg);
	clearTable();
	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);
	printf("-----------------------\n");
	printTable();









	sqlite3_close(db);
	cout << "Press any key to continue . . .\n";
	_getch();
	return 0;

}
bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	clearTable();
	string price = "", balance,ava;
	string s = "select * From cars join accounts where cars.id = ";
	s += std::to_string(carid);
	s += " and accounts.Buyer_id=";
	s += std::to_string(buyerid);
	const char * c = s.c_str();
	sqlite3_exec(db, c, callback, 0, &zErrMsg);
	if (results.size()==0)
		return false;

	for (auto it = results.begin(); it != results.end(); ++it)
	{
		string col = it->first;
		if (col =="price")
		{
			price = it->second.at(0);
		}
		else if (col == "balance")
		{
			balance = it->second.at(0);
		}
		else if (col == "available")
		{
			ava = it->second.at(0);
		}
	}
	if (ava == "0")
	{
		return false;
	}
	if (atoi(balance.c_str()) < atoi(price.c_str()))
	{
		return false;
	}
	sqlite3_exec(db, "begin transaction" , NULL, 0, &zErrMsg);
	s = "update cars set available=0 where id=";
	s += to_string(carid);
	sqlite3_exec(db, s.c_str(), NULL, 0, &zErrMsg);

	s = "update accounts set balance=balance-";
	s += price;
	s += " where Buyer_id=";
	s += to_string(buyerid);
	sqlite3_exec(db, s.c_str(), NULL, 0, &zErrMsg);


	sqlite3_exec(db, "commit", NULL, 0, &zErrMsg);
	return true;
}
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	string s;
	rc = sqlite3_exec(db, "begin transaction", NULL, 0, &zErrMsg);
	if (rc)
	{
		return false;
	}
	s = "update accounts set balance=";
	s += "balance-";
	s += to_string(amount);
	s += " where id=";
	s += to_string(from);
	rc = sqlite3_exec(db, s.c_str(), NULL, 0, &zErrMsg);
	if (rc)
	{
		return false;
	}
	s = "update accounts set balance=";
	s += "balance+";
	s += to_string(amount);
	s += " where id=";
	s += to_string(to);
	rc = sqlite3_exec(db, "commit", NULL, 0, &zErrMsg);
	if (rc)
	{
		return false;
	}
	return true;
}